package com.ta.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.Keys;

public class MainPage extends BasePage {
    private final Logger logger = LogManager.getRootLogger();
    private final String PAGE_URL = "https://cloud.google.com/";

    @FindBy(xpath = "//div[@class='devsite-searchbox']")
    private WebElement searchButton;

    @FindBy(name = "q")
    private WebElement inputSearch;

    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public MainPage openPage() {
        driver.navigate().to(PAGE_URL);
        logger.info("cloud main page opened");
        return this;
    }

    public SearchResult search(String searchPattern) {
        logger.info("cloud search page opened");
        waitVisibilityOfElement(50, searchButton);
        searchButton.click();
        waitVisibilityOfElement(50, inputSearch);
        inputSearch.sendKeys(searchPattern + Keys.ENTER);
        return new SearchResult(driver);
    }

}
