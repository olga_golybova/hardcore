package com.ta.model;

public class Email {
    private String emailAddress;

    public Email(String emailAddress) {

        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

}
