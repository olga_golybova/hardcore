package com.ta.service;

import com.ta.model.Instance;

public class InstanceCreator {
    private static final String  TESTDATA_NUMBER_OF_INSTANCES = "testdata.instance.numberOfInstances";
    private static final String TESTDATA_OPERATING_SYSTEM_SOFTWARE = "testdata.instance.operatingSystemSoftware";
    private static final String TESTDATA_PROVISIONING_MODEL = "testdata.instance.provisioningModel";
    private static final String TESTDATA_SERIES = "testdata.instance.series";
    private static final String TESTDATA_MACHINE_TYPE = "testdata.instance.machineType";
    private static final String TESTDATA_ADD_GPU = "testdata.instance.addGPU";
    private static final String TESTDATA_NUMBER_OF_GPUS = "testdata.instance.numberOfGPUs";
    private static final String TESTDATA_TYPE_GPU = "testdata.instance.typeGPU";
    private static final String TESTDATA_LOCAL_SSD = "testdata.instance.localSSD";
    private static final String TESTDATA_DATACENTER_LOCATION = "testdata.instance.datacenterLocation";
    private static final String TESTDATA_COMMITED_USAGE = "testdata.instance.commitedUsage";

    public static Instance withDataFromProperty(){
        return new Instance(TestDataReader.getTestData(TESTDATA_NUMBER_OF_INSTANCES),
                TestDataReader.getTestData(TESTDATA_OPERATING_SYSTEM_SOFTWARE),
                TestDataReader.getTestData(TESTDATA_PROVISIONING_MODEL),
                TestDataReader.getTestData(TESTDATA_SERIES),
                TestDataReader.getTestData(TESTDATA_MACHINE_TYPE),
                TestDataReader.getTestData(TESTDATA_ADD_GPU),
                TestDataReader.getTestData(TESTDATA_NUMBER_OF_GPUS),
                TestDataReader.getTestData(TESTDATA_TYPE_GPU),
                TestDataReader.getTestData(TESTDATA_LOCAL_SSD),
                TestDataReader.getTestData(TESTDATA_DATACENTER_LOCATION),
                TestDataReader.getTestData(TESTDATA_COMMITED_USAGE));
    }
}
