package com.ta.driver;

import org.openqa.selenium.WebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Driver {

    private static WebDriver driver;

    private Driver() {
    }

    //static String browserToUse = System.setProperty("browser","firefox");

    public static WebDriver getDriver() {
        if (null == driver) {
            switch (System.getProperty("browser","")) {
                case "firefox": {
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    break;
                }
                default: {
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("--disable-blink-features");
                    options.addArguments("--disable-blink-features=AutomationControlled");
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                }
                driver.manage().window().maximize();
            }
        }
        return driver;
    }

    public static void closeDriver() {
        driver.quit();
        driver = null;
    }
}
