import com.ta.driver.Driver;
import com.ta.utils.TestListener;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public class CommonConditions {
    protected WebDriver driver;

    @BeforeMethod()
    public void setUp()
    {
        driver = Driver.getDriver();
    }

    @AfterMethod()
    public void stopBrowser()
    {
        Driver.closeDriver();
    }
}
