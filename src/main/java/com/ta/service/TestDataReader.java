package com.ta.service;

import java.util.ResourceBundle;

public class TestDataReader {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("staging");

    public static String getTestData(String key){
        return resourceBundle.getString(key);
    }
}
