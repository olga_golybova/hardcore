package com.ta.model;

public class Instance {

    private final String  numberOfInstances;
    private final String operatingSystemSoftware;
    private final String provisioningModel;
    private final String series;
    private final String machineType;
    private final String addGPU;
    private final String numberOfGPUs;
    private final String typeGPU;
    private final String localSSD;
    private final String datacenterLocation;
    private final String commitedUsage;

    public Instance(String numberOfInstances, String operatingSystemSoftware,
                    String provisioningModel, String series,
                    String machineType, String addGPU,
                    String numberOfGPUs, String typeGPU, String localSSD,
                    String datacenterLocation, String commitedUsage) {
        this.numberOfInstances = numberOfInstances;
        this.operatingSystemSoftware = operatingSystemSoftware;
        this.provisioningModel = provisioningModel;
        this.series = series;
        this.machineType = machineType;
        this.addGPU = addGPU;
        this.numberOfGPUs = numberOfGPUs;
        this.typeGPU = typeGPU;
        this.localSSD = localSSD;
        this.datacenterLocation = datacenterLocation;
        this.commitedUsage = commitedUsage;
    }

    public String getNumberOfInstances() {
        return numberOfInstances;
    }

    public String getOperatingSystemSoftware() {
        return operatingSystemSoftware;
    }

    public String getProvisioningModel() {
        return provisioningModel;
    }

    public String getSeries() {
        return series;
    }

    public String getMachineType() {
        return machineType;
    }

    public String isAddGPU() {
        return addGPU;
    }

    public String getNumberOfGPUs() {
        return numberOfGPUs;
    }

    public String getTypeGPU() {
        return typeGPU;
    }

    public String getLocalSSD() {
        return localSSD;
    }

    public String getDatacenterLocation() {
        return datacenterLocation;
    }

    public String getCommitedUsage() {
        return commitedUsage;
    }
}
