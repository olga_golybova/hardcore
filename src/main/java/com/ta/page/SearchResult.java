package com.ta.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResult extends BasePage{

    @FindBy(xpath = "//a//*[text()='Google Cloud Platform Pricing Calculator']")
    private WebElement calculatorLink;

    protected SearchResult(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public CalculatorPage openCalculator() {
        this.waitVisibilityOfElement(80,calculatorLink);
        calculatorLink.click();
        return new CalculatorPage(driver);
    }

}
